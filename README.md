ogol
====
Ogol is a clone of the [Logo programming language](https://el.media.mit.edu/logo-foundation/what_is_logo/logo_primer.html), a DSL for drawing [turtle graphics](https://en.wikipedia.org/wiki/Turtle_graphics), which are "vector graphics using a relative cursor (the "turtle") upon a Cartesian plane" (Wikipedia).

# Language
Unlike Logo itself, Ogol's syntax is Scheme-like, making use of S-expressions. In fact, Ogol programs themselves are simply one giant S-expression, and all the interpreter does is continously reduce S-expressions into simpler S-expressions! The decision criterion for doing so is simple:

- Atomic values (which are also S-expressions) evaluate to themselves
- S-expressions beggining with functions are treated as a function call with the rest of the elements passed as arguments
- All other S-expressions have each of their elements evaluated in sequence

For instance, a simple program might look like this:
```
(   
    "Since string values are atomic, their evaluation does not mutate state,"
    "so they can be used at the top level as comments"
    (define msg "73 * 42 is "
    (define res (* 73 42)))
    (print (+ msg res))
)
```

# Built-Ins
The main point of Ogol is drawing vector graphics to an onscreen GUI. In addition to basic printing and arithmetic operations, a few commands are 10 supported:
```
(forward 30)
```
Moves the turtle 30 pixels forward, leaving a straight line.
```
(speed 10)
```
Sets the speed of the turtle to 10 px/second. 
```
(rotate 30)
```
Turns the angle of the turtle 30 degrees counter-clockwise. 
```
(color "red")
```
Changes the color of the turtle.

# Installation
Clone the repository:
```
git clone https://gitlab.com/TimothyZhou/ogol.git
```
A `CMakeLists.txt` is provided. Run `ogol-lang-app`. The `Cinder` library must be installed.
